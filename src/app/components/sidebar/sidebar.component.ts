import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    // { path: '/pba_introducers/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    // { path: '/profile', title: 'profile',  icon:'ni-planet text-blue', class: '' },
    { path: '/cases', title: 'Cases',  icon:'ni ni-collection text-info', class: '' },
    // { path: '/Bank', title: 'Bank',  icon:'ni-planet text-blue', class: '' },
    // { path: '/pba_introducers/inbox', title: 'Inbox',  icon:'ni ni-email-83 text-success', class: '' },
    // { path: '/pba_introducers/upload', title: 'Upload',  icon:'ni ni-cloud-download-95 text-warning', class: '' },
    // { path: '/login', title: 'Login',  icon:'ni-key-25 text-info', class: '' },
    // { path: '/register', title: 'Register',  icon:'ni-circle-08 text-pink', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router,private ds:DataserviceService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

  Logout(){
    this.ds.logout();
    this.router.navigateByUrl('/login');
  }
}
