import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  public focus;
  public listTitles: any[];
  public location: Location;
  logininfo:any = {
    firstname:'firstname',
    lastname:'lastname'
  };

  user_data:any ={
    firstname:'firstname',
    surname:'surname'
  };
  constructor(location: Location,  private element: ElementRef, private router: Router,private ds:DataserviceService) {
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'))
    // console.log('Bearer '+' '+this.logininfo['success'].token);
    if(this.logininfo){
   this.ds.getrecord('profile','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
    this.user_data  =res['data'];
   })
  }
  //  console.log(this.user_data);
    this.location = location;
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);
    // console.log('lilength'+this.listTitles);
  }
  getTitle(){
    var titlee = this.location.prepareExternalUrl(this.location.path());

    if(titlee.charAt(0) === '#'){
     
        titlee = titlee.slice( 1 );
        if(titlee == 'pba_introducers/cases'){
          return 'Add cases';
        }
    }
    var s = titlee;
   
    s = s.substring(0, s.indexOf('?'));
    s.slice( 1 );
   
    if(s.charAt(0) === '/'){
     let tit = s.slice( 1 );
     if(tit == 'pba_introducers/addcase'){
      return 'Edit cases';
    }else if(tit == 'pba_introducers/addinbox'){
      return 'Edit Inbox';
    }
    }
   
    

    for(var item = 0; item < this.listTitles.length; item++){ 
      // console.log(titlee);
        if(this.listTitles[item].path === titlee){
            return this.listTitles[item].title;
        }else if(titlee.charAt(0) === '/'){
          let tit = titlee.slice( 1 );
          if(tit == 'pba_introducers/addcase'){
            return 'Add case';
          }else if(tit == 'pba_introducers/addinbox'){
            return 'Add Inbox';
          }else if(tit == 'pba_introducers/send'){
            return 'Send';
          }
        }

     
    }
    return 'Cases';
  }

  Logout(){
    this.ds.logout();
    this.router.navigateByUrl('/pba_introducers/login');
  }

}
