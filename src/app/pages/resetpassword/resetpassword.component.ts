import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
import { error } from 'protractor';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  isSubmitted = false;
  resetpasswordform:FormGroup;
  constructor(private ds:DataserviceService,private fb:FormBuilder,private route:ActivatedRoute,private router:Router,private location:Location) { 
    this.resetpasswordform = this.fb.group({
      token:[''],
      email:[''], 
      password:['',[Validators.required,,Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{9,})/)]],
      password_confirmation:['',Validators.required]
    });
    this.route.queryParams.subscribe(res => {
    //  this.resetpasswordform.controls['token'].setValue(res['token']);
    this.ds.getresetrecord('find/'+res['token']).subscribe(res => {
      // console.log(res['token']);
      if(res['message']){
        Swal.fire({
          title:'Error',
          text:res['message'],
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => { 
          if (result.value) {
            this.router.navigateByUrl("/forgotpassword", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/forgotpassword');
            });
          }
        })
      }else{
        // alert(res['token']);
    this.resetpasswordform.controls['token'].setValue(res['token']);
    this.resetpasswordform.controls['email'].setValue(res['email']);
      }
    },error => {
      if(error.status == 404){
        Swal.fire({
          title:'Error',
          text:error['error'].message,
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/forgotpassword", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/forgotpassword');
            });
          }
        })
      }
    })
    }); 

  }

  get formControls() { return this.resetpasswordform.controls; }
  ngOnInit(): void {
  }

  resetpassword(){
    this.isSubmitted = true;
    if(this.resetpasswordform.invalid){
      return;
    }
    this.ds.loginpost('reset',this.resetpasswordform.value).subscribe(res=>{
      console.log(res);
      if(res['success'] == true){
      Swal.fire({
        title:'Success',
        text:res['message'],
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/login", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
            this.router.navigateByUrl('/login');
          });
        }
      })
    }
    },error => {
      if(error.status == 422){
        Swal.fire({
          title:'Error',
          text:error['error'].message,
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/resetpassword", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/resetpassword');
            });
          }
        })
      }
    })

  }

}
