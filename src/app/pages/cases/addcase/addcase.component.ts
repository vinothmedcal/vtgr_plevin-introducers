import { Component, OnInit, ɵConsole } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { FormGroup, FormBuilder, Validators,FormArray, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
import { addresses,postcodes,keys } from "@ideal-postcodes/api-fixtures";
import * as $ from 'jquery';
import { Observable,of } from 'rxjs';
import {debounceTime, map,distinctUntilChanged,switchMap,catchError,tap,finalize} from 'rxjs/operators';
import { HttpClient,HttpParams } from '@angular/common/http';
import { error } from 'protractor';
import { resolve } from 'dns';
import { rejects } from 'assert';


@Component({
  selector: 'app-addcase',
  templateUrl: './addcase.component.html',
  styleUrls: ['./addcase.component.scss']
})
export class AddcaseComponent implements OnInit {
  postcodeempty = false;
  isLoading = false;
  keyword = 'postcode';
  title = 'Add Case';
  submittitle = 'Save';
  postcode: FormControl = new FormControl();
  isSubmitted = false;
  addcseform:FormGroup;
  editcasedata:any;
  logininfo:any;
  caseid:any;
  fileData: File = null;
  file :any = [];
  documentarray:FormArray;
  postcodearraydata:any;
  searching = false;
  searchFailed = false;
  filterarray:any;
  filterpostcodearray:any[] = [];
  filteraddressarray:any[] =[];
  radioSelected:boolean=false;
  fileDatas:any=[];
  user_data:any;
  constructor(private ds:DataserviceService,private fb: FormBuilder,private router: ActivatedRoute,private route:Router,private location:Location,private http:HttpClient) {
  
    // this.ds.getpostcode('https://api.ideal-postcodes.co.uk/v1/postcodes/ID11QD?api_key=ak_k8pwwjngF2y9rTbOzKJK0dDTSzTK5').subscribe(res =>{
    //   console.log(res);
    // })

    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.ds.getrecord('profile','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      this.user_data  =res['data'];
      this.addcseform.controls['cf_leads_username'].setValue(res['data'].username);
     })
    this.router.queryParams.subscribe(res => {
      this.caseid = res.Case_id;
    // this.ds.getrecord('cases/'+res.Case_id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
    //   // this.addcseform.setValue(res['success'].data)
    //   console.log(res['success']['data'].title);
    //   this.formControls.title.setValue((res['success']['data'].title));
    //   this.formControls.firstname.setValue((res['success']['data'].firstname));
    //   this.formControls.surname.setValue((res['success']['data'].surname));
    //   this.formControls.dob.setValue((res['success']['data'].dob));
    //   this.formControls.postcode.setValue((res['success']['data'].postcode));
    //   this.formControls.email.setValue((res['success']['data'].email));
    //   this.formControls.mobile.setValue((res['success']['data'].mobile));
    //   this.formControls.phone.setValue((res['success']['data'].phone));
    //   this.formControls.address.setValue((res['success']['data'].address));
    //   this.formControls.bank.setValue((res['success']['data'].bank));
    //   this.formControls.account_type.setValue((res['success']['data'].account_type));
    //   this.formControls.moved_address.setValue((res['success']['data'].moved_address));
    //   this.formControls.info_pack.setValue((res['success']['data'].info_pack));
    //   this.formControls.referred_cta.setValue((res['success']['data'].referred_cta));
      
    // })
    });
    if(this.caseid){
this.title = 'Edit Case';
this.submittitle = 'Update';
    }
  this.getvtigerAPI();
  console.log(this.getvtigerAPI());
   }

   getvtigerAPI(): string {
    return sessionStorage.getItem('vtiger_api');
  }

   numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngOnInit(){
  
    this.addcseform = this.fb.group({ 
      id:new FormControl(''),
      __vtrftk: new FormControl('sid:9890730014c02dd2399ba08405c813ddfd7ca898,1586883233'),
      publicid:new FormControl('6aa6ade15d9666f1e8e5cc271d12c62c'),
      urlencodeenable:new FormControl('1'),
      name:new FormControl('Plevin Introducers'),
      __vtCurrency: new FormControl('1'),
      fld_vtcmctaname: new FormControl('Plevin',Validators.required),
      fld_title_1799: new FormControl('Dr.',Validators.required),
      fld_firstname:new FormControl('',Validators.required),
      fld_surname:new FormControl('',Validators.required),
      fld_dateofbirth:new FormControl('',Validators.required),
      fld_postcode:new FormControl('',Validators.required),
      postcode1:new FormControl('0',Validators.required),
      fld_emailaddress:new FormControl('',Validators.required),
      fld_mobilenumber:new FormControl('',Validators.required),
      fld_phonenumber:new FormControl('',Validators.required),
      fld_address:new FormControl('',Validators.required),
      fld_bank:new FormControl('',Validators.required),
      fld_accounttype:new FormControl('',Validators.required),
      fld_movedaddresssince_1821:new FormControl('yes'),
      fld_didyoureceiveaninfopack_1823:new FormControl('yes'),
      fld_happytobereferredtocta_1825: new FormControl('yes'),
      fld_uploaddocument: new FormControl('',Validators.required),
      fld_documentdescription:new FormControl('',Validators.required),
      cf_vtcmcta_casestatus: new FormControl('New',Validators.required),
      cf_leads_username:new FormControl('')
    })
    this.documentarray = this.addcseform.get('document_array') as FormArray;

  }


  // search = (text$: Observable<string>) =>{
  
  //   return text$.pipe( 
  //     debounceTime(300),
  //     distinctUntilChanged(),
  //     switchMap(term => this.ds.getpostcode('https://api.ideal-postcodes.co.uk/v1/postcodes/'+term+'?api_key=ak_k8pwwjngF2y9rTbOzKJK0dDTSzTK5')),
  //         // catchError(new ErrorInfo().parseObservableResponseError)
  //    );
  //     }

      resultFormatBandListValue(value: any) {            
        return value.postcode;
      } 
      /**
        * Initially binds the string value and then after selecting
        * an item by checking either for string or key/value object.
      */
      inputFormatBandListValue(value: any)   {
        if(value.postcode)
          return value.postcode
        return value;
      }

    

  // onchange($event,text){
  //   if(text === 'postcode'){
  //    this.http.get('https://api.ideal-postcodes.co.uk/v1/postcodes/'+$event+'?api_key=ak_k8s7tfokQZDY09RMMCCUMaiYd3Pje').subscribe(data => {
  //     if (data['result'] == undefined) {
  //       // this.errorMsg = data['Error'];
  //       this.filterarray = [];
  //     } else {
  //       // this.errorMsg = "";
  //       console.log(data['result']);
  //       this.filterarray = data['result'];
  //     }

  //     console.log(this.filterarray);
  //   });
  // }else {
  //   this.http.get('https://api.ideal-postcodes.co.uk/v1/autocomplete/addresses?api_key=ak_k8s7tfokQZDY09RMMCCUMaiYd3Pje='+$event).subscribe(data => {
  //     if (data['result'].hits == undefined) {
  //       // this.errorMsg = data['Error'];
  //       this.filteraddressarray = [];
  //     } else {
  //       // this.errorMsg = "";
  //       // console.log(data['result'].hits);
  //       this.filteraddressarray = data['result'].hits;
  //     }

  //     console.log(this.filteraddressarray);
  //   });
  // }
  // }

  get formControls() { return this.addcseform.controls; }

  get documentarraycontrol(): FormArray {
    return this.addcseform.get('document_array') as FormArray;
  } 

  // selectEvent(event){
  //   this.ds.getpostcode('postcodes/',event.target.value+'?api_key=ak_k8s7tfokQZDY09RMMCCUMaiYd3Pje').subscribe(data => {
  //     console.log(data);
  //     this.postcodearraydata = data['result'];
  //       // this.myBooks = data as any[];
  //       //console.log(data[0].BookName);
  //   })
  // }
  onAddressChange(address:any) {
    console.log(address);
    let line =address.line_1+' ';
    if(address.line_2) {
      line+=address.line_2+ ' ';
    }
    if(address.line_3) {
      line+=address.line_3;
    }
    this.formControls.fld_address.setValue(line+ ' '+address.district+' '+address.county+' '+' '+address.country);
  }

  findAddress(value) {
    // console.log(this.addcseform.controls['postcode'].value);
    // this.isSubmitted = true;
    // console.log(this.addcseform.controls['postcode'].invalid)
    if(this.addcseform.controls['fld_postcode'].invalid){
      this.postcodeempty = true;
    }else{
      this.postcodeempty = false;
    this.isLoading = true;
    this.ds.getpostcode('addresses?api_key=','&query='+value).subscribe(data => {
      this.isLoading = false;
      console.log(data);
      this.filteraddressarray = data['result'].hits;
      if( this.filteraddressarray.length >0) {
        this.radioSelected =false;
      }else{
        this.radioSelected=true;
      }
    
        // this.myBooks = data as any[];
        //console.log(data[0].BookName);
    },error => {
      if(error.status == 0){
        this.isLoading = false;
      }
    })
  }
   }
  // postaddressselect(event){
  //   this.ds.getpostcode('addresses?api_key=ak_k8s7tfokQZDY09RMMCCUMaiYd3Pje&query=',event.target.value).subscribe(data => {
  //     console.log(data);
  //     this.filteraddressarray = data['result'].hits;
  //       // this.myBooks = data as any[];
  //       //console.log(data[0].BookName);
  //   })
  // }


  // search(term: string) {
  //   // if (term === '') {
  //   //   return of([]);
  //   // } 
  //   this.ds.getpostcode('postcodes/'+ term +'?api_key=ak_k8pwwjngF2y9rTbOzKJK0dDTSzTK5').subscribe(res => {
  //     console.log(res);
  //   })
  // }
  
  upload_document_controls(){
    return new FormGroup({
      document: new FormControl('',Validators.required),
      description: new FormControl('',Validators.required)
    });
  }

  adddocument_control(){
    let document_control = this.addcseform.get('document_array') as FormArray;
    document_control.push(this.upload_document_controls ());
  }

  close_document(i){
    // console.log(index);
    this.documentarray.removeAt(i);
  }

 

  addcase(){  
    this.isSubmitted = true;
    // alert(this.filteraddressarray.length);
    
    if(this.addcseform.invalid){
      // console.log(this.radioSelected);
     
      return;
    }
    
    if(this.postcodeempty){
      return;
    }else{
      this.isLoading =true;
      if(this.addcseform.get('id').value){
        this.ds.postRecords('cases/'+this.addcseform.get('id').value,this.addcseform.value).subscribe(res=>{
          this.isLoading = false;
          if(res['success'].message){
          Swal.fire({
            title:'Updated.',
            text:res['success'].message,
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.route.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.route.navigate([decodeURI(this.location.path())])
                this.addcseform.reset();
                this.route.navigateByUrl('/cases');
              });
            }
          })
        }
          // alert(res['success'].message);
          // this.addcseform.reset();
          // this.route.navigateByUrl('/cases');
        })
      }else{
      
        const myFormData = new FormData();
        myFormData.append('__vtrftk',  this.addcseform.value.__vtrftk);
        myFormData.append('publicid',  this.addcseform.value.publicid);
        myFormData.append('urlencodeenable',  this.addcseform.value.urlencodeenable);
        myFormData.append('name',  this.addcseform.value.name);
        myFormData.append('__vtCurrency',  this.addcseform.value.__vtCurrency);
        myFormData.append('fld_vtcmplevinintroducersname',  this.addcseform.value.fld_vtcmctaname);
        myFormData.append('salutationtype',  this.addcseform.value.fld_title_1799);
        myFormData.append('firstname',  this.addcseform.value.fld_firstname);
        myFormData.append('lastname',  this.addcseform.value.fld_surname);
        myFormData.append('cf_leads_clientdateofbirth',  this.addcseform.value.fld_dateofbirth);
        myFormData.append('city',  this.addcseform.value.fld_postcode);
        myFormData.append('email',  this.addcseform.value.fld_emailaddress);
        myFormData.append('mobile',  this.addcseform.value.fld_mobilenumber);
        myFormData.append('phone',  this.addcseform.value.fld_phonenumber);
        myFormData.append('cf_leads_addressline1',  this.addcseform.value.fld_address);
        myFormData.append('cf_leads_bank',  this.addcseform.value.fld_bank);
        myFormData.append('cf_leads_accounttype',  this.addcseform.value.fld_accounttype);
        myFormData.append('cf_leads_movedaddresssince',  this.addcseform.value.fld_movedaddresssince_1821);
        myFormData.append('cf_leads_didyoureceiveaninfopack',  this.addcseform.value.fld_didyoureceiveaninfopack_1823);
        myFormData.append('cf_leads_happytobereferredtocta',  this.addcseform.value.fld_happytobereferredtocta_1825);
        myFormData.append('file_4_1',  this.addcseform.value.fld_uploaddocument);
        myFormData.append('cf_leads_documentdescription',  this.addcseform.value.fld_documentdescription);
        myFormData.append('cf_leads_casestatus',this.addcseform.value.cf_vtcmcta_casestatus);
        myFormData.append('cf_leads_username',this.addcseform.value.cf_leads_username);
      
      const endpoint = this.getvtigerAPI();
        fetch(endpoint,{
          method:'post',
          body: myFormData,
          mode: 'no-cors'
        }).then((response) =>{
          if(response){
          Swal.fire({
            title:'Success.',
            text:"Successfully Added.",
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.route.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.route.navigate([decodeURI(this.location.path())])
                this.addcseform.reset();
                this.route.navigateByUrl('/cases');
              });
            }
          })
        }
          return response;
        }).then((res) => {
          return res;
      });
        // this.ds.vitigerpost('modules/Webforms/capture.php',myFormData).subscribe(res=>{
        //   // console.log(res);
        //   // return;
        //   this.isLoading = false;
        //   if(res['success'] == true){
        //   Swal.fire({
        //     title:'Success.',
        //     text:"Successfully Added.",
        //     showConfirmButton:true,
        //     confirmButtonText:'Ok'
        //   }).then((result) => {
        //     if (result.value) {
        //       this.route.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
        //         console.log([decodeURI(this.location.path())]);
        //         this.route.navigate([decodeURI(this.location.path())])
        //         this.addcseform.reset();
        //         this.route.navigateByUrl('/cases');
        //       });
        //     }
        //   })
        // }
      
        // })
      }
    }
   
  }

  createItem(data): FormGroup {
    return this.fb.group(data);
}

  get documenfiletarray(): FormArray {
    // console.log(this.demoForm);
    return this.addcseform.get('document_array') as FormArray;
  };

  fileProgress(fileInput: any) {
    console.log(fileInput);
    let fileData = fileInput.target.files[0];
this.file = fileData;
// let file =this.formControls.document_array.value[index];
// file.document=this.file;
// file.index=index;
// if(this.fileDatas[index]) {
//   this.fileDatas[index]=file;
// } else {
//   this.fileDatas.push(file);
// }
// console.log(this.fileDatas);
}


}