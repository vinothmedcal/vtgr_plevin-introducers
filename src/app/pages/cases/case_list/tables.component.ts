import { Component, OnInit ,OnDestroy} from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { Router } from '@angular/router';
import { empty } from 'rxjs';
import { count } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import Swal from 'sweetalert2'
import { error } from 'protractor';

// import 'rxjs/add/operator/map';
@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger:Subject<any> = new Subject();
  caselist_array:any;
  logininfo: any;
  nodata:boolean = false;
  constructor(private ds:DataserviceService,private router:Router,private location:Location) {
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    // console.log('Bearer '+' '+logininfo['success'].token);
  }

  ngOnInit() {
    this.caselist_array = '';
    this.getcaselist();


  }

  editcase(data){
    const navigationExtras = {
      queryParams: {
          Case_id: data      
      }
  };
this.router.navigate(['/addcase'], navigationExtras);
  
  }

  deletecase(id){
this.ds.deleterecord('cases/'+id).subscribe(res => {
  if(res['success'].status === 'success'){
    Swal.fire({
      title:'Success.',
      text:res['success'].message,
      showConfirmButton:true,
      confirmButtonText:'Ok'
    }).then((result) => {
      if (result.value) {
        this.caselist_array = [];
        this.getcaselist();
        this.router.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
          console.log([decodeURI(this.location.path())]);
          this.router.navigate([decodeURI(this.location.path())])
          this.router.navigateByUrl('/cases');
        });
      }
    })
  }else{
    Swal.fire({
      title:'Error',
      text:"Something went wrong!",
      showConfirmButton:true,
      confirmButtonText:'Ok'
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
          console.log([decodeURI(this.location.path())]);
          this.router.navigate([decodeURI(this.location.path())])
          this.router.navigateByUrl('/cases');
        });
      }
    })
  }
},error => {
  Swal.fire({
    title:'Error',
    text:error,
    showConfirmButton:true,
    confirmButtonText:'Ok'
  }).then((result) => {
    if (result.value) {
      this.router.navigateByUrl("/pba_introducers/cases", { skipLocationChange: true }).then(() => {
        console.log([decodeURI(this.location.path())]);
        this.router.navigate([decodeURI(this.location.path())])
        this.router.navigateByUrl('/pba_introducers/cases');
      });
    }
  })
})
}

  getcaselist(){
    this.ds.getrecord('cases','Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      let data = res['success'].data;
      let objectcase = [];
      data.forEach(e => {
        objectcase.push(e);
      })
      if(objectcase.length > 0){
        this.caselist_array = objectcase;
      }else{
       this.nodata = true;
      }
    
    })
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }


  

}
