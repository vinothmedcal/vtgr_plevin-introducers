import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-addinbox',
  templateUrl: './addinbox.component.html',
  styleUrls: ['./addinbox.component.scss']
})
export class AddinboxComponent implements OnInit {
  isSubmitted = false;
  title = 'Add Inbox';
  submittitle = 'Save';
  addinboxform:FormGroup;
  logininfo:any;
  inboxid:any;
  user_data:any;
  userdata:any;
  constructor(private ds:DataserviceService,private fb: FormBuilder,private router: ActivatedRoute,private route:Router,private location:Location) {
  
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.router.queryParams.subscribe(res => {
      this.inboxid = res.inbox_id;
    this.ds.getrecord('inbox/'+res.inbox_id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      this.addinboxform.setValue(res['success'].data)
    })
    });
    if(this.inboxid){
this.title = 'Edit Case';
this.submittitle = 'Update';
    }
    this.ds.getrecord('profile','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log(res['data'].email);
      this.addinboxform.controls['user_email'].setValue(res['data'].email);
     });
     this.getuseremaildata(); 
 
   }

  ngOnInit(): void {
    this.addinboxform = this.fb.group({
      id:[''],
      date:['',Validators.required],
      to:['',Validators.required],
      subject:['',Validators.required],
      message:['',Validators.required],
      user_email:['']
    })
  }

  getuseremaildata(){
    this.ds.getrecord('email','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      console.log(res);
      let userarray = [];
      let data = res['success'].data;
      data.forEach(e => {
          userarray.push({id:e.id,email:e.email});
      });
      this.userdata = userarray;
     })
  }

get formControls() { return this.addinboxform.controls; }

  addinbox(){
    this.isSubmitted = true;
    if(this.addinboxform.invalid){
      return;
    }else{
      if(this.addinboxform.get('id').value){
        this.ds.postRecords('new/'+this.addinboxform.get('id').value,this.addinboxform.value).subscribe(res=>{
          Swal.fire({
            title:'Success.',
            text:res['success'].message,
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.route.navigateByUrl("/pba_introducers/inbox", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.route.navigate([decodeURI(this.location.path())])
                this.addinboxform.reset();
                this.route.navigateByUrl('/pba_introducers/inbox');
              });
            }
          })
          // alert(res['success'].message);
          // this.addinboxform.reset();
          // this.route.navigateByUrl('/inbox');
        })
      }else{
        this.ds.postRecords('new',this.addinboxform.value).subscribe(res=>{
          Swal.fire({
            title:'Success.',
            text:res['success'].status,
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.route.navigateByUrl("/pba_introducers/cases", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.route.navigate([decodeURI(this.location.path())])
                this.addinboxform.reset();
         this.route.navigateByUrl('/pba_introducers/inbox');
              });
            }
          })
        //  alert(res['success'].status);
        //  this.addinboxform.reset();
        //  this.route.navigateByUrl('/inbox');
        })
      }
    }
  }

}
