import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddinboxComponent } from './addinbox.component';

describe('AddinboxComponent', () => {
  let component: AddinboxComponent;
  let fixture: ComponentFixture<AddinboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddinboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
