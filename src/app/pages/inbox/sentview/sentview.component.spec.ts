import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentviewComponent } from './sentview.component';

describe('SentviewComponent', () => {
  let component: SentviewComponent;
  let fixture: ComponentFixture<SentviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
