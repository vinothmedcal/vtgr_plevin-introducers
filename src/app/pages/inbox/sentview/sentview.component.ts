import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';

@Component({
  selector: 'app-sentview',
  templateUrl: './sentview.component.html',
  styleUrls: ['./sentview.component.scss']
})
export class SentviewComponent implements OnInit {
  sendid:any;
  logininfo:any;
  sendarraydata
  constructor(private router:Router,private route:ActivatedRoute,private ds:DataserviceService) {
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.route.queryParams.subscribe(res => {
      this.sendid = res.send_id;
    this.ds.getrecord('sent/'+res.send_id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log( res['success'].data);
      let sendarray = [];
      sendarray.push(res['success'].data);
     this.sendarraydata = sendarray[0];
    //  console.log(this.inboxarraydata[0].subject);
    })
    });
   }

  ngOnInit(): void {
  }

  

}
