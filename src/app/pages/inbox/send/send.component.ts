import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.scss']
})
export class SendComponent implements OnInit {
  logininfo:any;
  sendid:any;
  senddata:any;
  nodata = false;
  userdata:any;
  constructor(private ds:DataserviceService,private router: ActivatedRoute,private route:Router,private location:Location) { 
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.ds.getrecord('profile','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      // this.getsend_data(res['data'].id);
       });
    this.router.queryParams.subscribe(res => {
      this.sendid = res.send_id;
    });
    this.getuseremaildata();
    this.getsenddata();
 
  }

  ngOnInit(): void {
  }
  getuseremaildata(){
    this.ds.getrecord('email','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log(res);
      let userarray = [];
      let data = res['success'].data;
      data.forEach(e => {
          userarray.push({id:e.id,email:e.email});
      });
      this.userdata = userarray;
     })
  }

  getsenddata(){
    this.ds.getrecord('sent','Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log(res);
     let data = res['success'].data;
     let objectinbox = [];
     let udata =  this.userdata;
     data.forEach(e => {
      let email = udata.filter(personObj => personObj.id == e.to);
   objectinbox.push({id:e.id,date:e.date,user_email:e.user_email,to:email[0].email,subject:e.subject,message:e.message});
     })
     if(objectinbox.length > 0){
       this.senddata = objectinbox;
     }else{
      this.nodata = true;
     }
  
    })
  }

getsend_data(id){
  this.ds.getrecord('sent/'+id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
    console.log(res['success'].status);
    if(res['success'].status == "success"){
      this.senddata = res['success'].data;
    }else{
      this.nodata = true;
    }
    
    
  })
}
   
  senddelete(id){
    // console.log(this.logininfo['success'].token);
    this.ds.postRecords('sent/'+id,this.logininfo['success'].token).subscribe(res => {
      Swal.fire({
        title:'Success.',
        text:res['success'].message,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.senddata = [];
          this.getsenddata();
          this.route.navigateByUrl("/pba_introducers/send", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.route.navigate([decodeURI(this.location.path())])
            this.route.navigateByUrl('/pba_introducers/send');
          });
        }
      })
      // alert(res['success'].message);
    })

  }

  send_view(id){
    const navigationExtras = {
      queryParams: {
          send_id: id      
      }
  };
this.route.navigate(['/pba_introducers/sentview'], navigationExtras);
  }

}
