import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,FormArray,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import { error } from 'protractor';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  isLoading = false;
  ErrorMessage = "";
  myOptions: IAngularMyDpOptions = {
    dateFormat: 'dd/mm/yyyy'
    // other options...
  };
  registrationForm:FormGroup;
  isSubmitted = false;
  postcodearraydata:any;
  filteraddressarray:any;
  constructor(private router: Router,private fb: FormBuilder,private ds:DataserviceService,private location:Location) { }

  ngOnInit() {
    let model: IMyDateModel = {isRange: false, singleDate: {jsDate: new Date()}, dateRange: null};
    this.registrationForm = this.fb.group({
      username: ['',Validators.required],
      firstname:['',Validators.required],
      surname:['',Validators.required],
      dob:['',Validators.required],
      postcode:['',Validators.required],
      email:['',Validators.required],
      mobile:['',Validators.required],
      phone:['',Validators.required],
      password:['',[Validators.required,,Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{9,})/)]],
      title:['1',Validators.required],
      address:['',Validators.required   ]

    })
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  get formControls() { return this.registrationForm.controls; }
  setDate(): void {
    // Set today date using the patchValue function
    let model: IMyDateModel = {isRange: false, singleDate: {jsDate: new Date()}, dateRange: null};
    this.registrationForm.patchValue({dob: model});
  }
  clearDate(): void {
    // Clear the date using the patchValue function
    this.registrationForm.patchValue({dob: null});
  }

  signup(){
    // console.log(this.registrationForm.value);
    this.isSubmitted = true;
    if(this.registrationForm.invalid){
      return;
    }
    this.isLoading = true;
   this.ds.loginpost('register',this.registrationForm.value).subscribe(res =>{
     this.isLoading = false;
     if(res['status'] == 'success'){
      Swal.fire({
        title:'Success',
        text:'Register Success.',
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/login", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
            this.router.navigateByUrl('/login')
        
          });
        }
      })
      //  alert('Register Success!');
      // this.router.navigateByUrl('/login')
     }else if(res['validation errors'].password){
      Swal.fire({
        title:'Error',
        text:res['validation errors'].password,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/register", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
        
          });
        }
      })
      //  alert((res['validation errors'].password));
     }else if(res['validation errors'].username){
      Swal.fire({
        title:'Error',
        text:res['validation errors'].username,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/register", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
        
          });
        }
      })
      //  alert((res['validation errors'].password));
     }
     else if(res['validation errors'].email){
      Swal.fire({
        title:'Error',
        text:res['validation errors'].email,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/register", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
        
          });
        }
      })
      //  alert((res['validation errors'].password));
     }
   },error => {
    if (error.status == 404)
    this.ErrorMessage = "You cleared browser history. Please refresh the page...";
  else
    this.ErrorMessage = "net::ERR_CONNECTION_REFUSED";
    this.isLoading =false;
   })
    
  }



}
