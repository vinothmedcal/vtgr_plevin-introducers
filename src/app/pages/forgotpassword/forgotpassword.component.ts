import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { error } from 'protractor';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  isSubmitted = false;
  forgotpasswpordform: FormGroup;
  constructor(private ds:DataserviceService,private fb: FormBuilder,private router:Router,private location:Location) { 

  }

  ngOnInit(): void {
    this.forgotpasswpordform = this.fb.group({
      email:['',Validators.required]
    })
  }

  get formControls() { return this.forgotpasswpordform.controls; }

  forgotpassword(){
    this.isSubmitted = true;
    if(this.forgotpasswpordform.invalid){
      return;
    }
    this.ds.loginpost('forgot',this.forgotpasswpordform.value).subscribe(res => {
     if(res['message']){
      Swal.fire({
        title:'Success',
        text:res['message'],
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/forgotpassword", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
            this.router.navigateByUrl('/forgotpassword');
          });
        }
      })
     }
    },error => {
      console.log(error);
      if(error['status'] == 404){
      Swal.fire({
        title:'Error',
        text:error['error'].message,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/forgotpassword", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
            this.router.navigateByUrl('/forgotpassword');
          });
        }
      })
    }else if(error['status'] == 422){
      Swal.fire({
        title:'Error',
        text:error['error']['errors'].email,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/forgotpassword", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
            this.router.navigateByUrl('/forgotpassword');
          });
        }
      })
    }
    })

  }

}
