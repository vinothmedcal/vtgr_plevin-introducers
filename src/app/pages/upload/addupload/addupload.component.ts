import { Component, OnInit } from '@angular/core';
import {
  HttpClient, HttpClientModule, HttpRequest, HttpResponse, HttpEvent,HttpEventType
} from "@angular/common/http";
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs'
import { ngfModule, ngf } from "angular-file"
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
@Component({
  selector: 'app-addupload',
  templateUrl: './addupload.component.html',
  styleUrls: ['./addupload.component.scss']
})
export class AdduploadComponent implements OnInit {
  isSubmitted = false;
  adddocumentform:FormGroup;
  fileData: File = null;
  file:any;
  constructor(public HttpClient:HttpClient,private fb:FormBuilder,private location:Location, private ds:DataserviceService,private router:Router) { }

  ngOnInit(): void {
    this.adddocumentform = this.fb.group({
      id:[''],
      document:['',Validators.required],
      date:['',Validators.required],
      description:['',Validators.required],
      title:['',Validators.required]
    })
  }

  fileProgress(fileInput: any) {
    console.log(fileInput);
    let fileData = fileInput.target.files[0];
    this.file=fileData;
    // let arr = fileData.split('/'); 
    // console.log(arr);
  }
  get formControls() { return this.adddocumentform.controls; }
  // onSubmit() {
  //   console.log(this.fileData);
  //   const formData = new FormData();
  //   formData.append('file', this.fileData);
  //   this.HttpClient.post('url/to/your/api', formData,{
  //     reportProgress:true,
  //     observe:'events'
  //   })
  //     .subscribe(res => {
  //       if(res.type == HttpEventType.UploadProgress) {
  //         console.log('Upload progress: ', Math.round(res.loaded / res.total * 100) + '%');
  //     } else if(res.type === HttpEventType.Response) {
  //         console.log(res);
  //     }
  //     })
  // }


  adddocument(){
    this.isSubmitted = true;
    this.adddocumentform.value.image=this.file;
    if(this.adddocumentform.invalid){
      return;
    }else{
      const myFormData = new FormData();
    myFormData.append('document', this.file,this.file.name);
    myFormData.append('title',  this.adddocumentform.value.title);
    myFormData.append('date',  this.adddocumentform.value.date);
    myFormData.append('description',  this.adddocumentform.value.description);
  
        this.ds.postRecords('document', myFormData,true).subscribe(res=>{
          // console.log(res['success'].status);
          if(res['success'].status === 'success'){
            Swal.fire({
              title:'Success',
              text:"Dodument upload "+' '+res['success'].status,
              showConfirmButton:true,
              confirmButtonText:'Ok'
            }).then((result) => {
              if (result.value) {
                this.router.navigateByUrl("/upload", { skipLocationChange: true }).then(() => {
                  console.log([decodeURI(this.location.path())]);
                  this.router.navigate([decodeURI(this.location.path())])
                  this.router.navigateByUrl('/upload');
                });
              }
            })
          }else if(res['validation_errors'].document){
            Swal.fire({
              title:'Error',
              text:res['validation_errors'].document,
              showConfirmButton:true,
              confirmButtonText:'Ok'
            }).then((result) => {
              if (result.value) {
                this.router.navigateByUrl("/upload", { skipLocationChange: true }).then(() => {
                  console.log([decodeURI(this.location.path())]);
                  this.router.navigate([decodeURI(this.location.path())])
                  this.router.navigateByUrl('/addupload');
                });
              }
            })
        //  alert( res['success'].status);
        //  this.adddocumentform.reset();
        //  this.router.navigateByUrl('/upload');
          }else{
            alert('Someting went wrong!');
          }
        })
      
    }
  }
}
