import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  validation_error:boolean = false;
  updateprofileform:FormGroup;
  logininfo:any;
  user_data:any;
  errors:any;

  constructor(private ds:DataserviceService,private fb: FormBuilder,private router:Router,private location:Location) {
    this.updateprofileform = this.fb.group({
      id:[''],
      username:['',Validators.required],
      // password:['',Validators.required],
      title:['',Validators.required],
      firstname:['',Validators.required],
      surname:['',Validators.required],
      dob:['',Validators.required],
      postcode:['',Validators.required],
      address:['',Validators.required],
      email:['',Validators.required],
      email_verified_at:[''],
      created_at:[''],
      updated_at:[''],
      phone:['',Validators.required],
      mobile:['',Validators.required],
    });
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'))
    // console.log('Bearer '+' '+this.logininfo['success'].token);
    
   this.ds.getrecord('profile','Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
     this.updateprofileform.setValue(res['data']);
    this.user_data = res['data'];
   })
   
   }

   numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngOnInit() {
    // console.log(this.user_data);
    // this.updateprofileform.('username').setValue('test');

    // console.log  (this.user_data);
  //  this.updateprofileform.patchValue({
  //   username:this.user_data.username
  //  });
  }

  updateprofile(){
this.ds.postRecords('profile',this.updateprofileform.value).subscribe(res => {

  if(res['validation errors']){
  if(res['validation errors'].email){
    Swal.fire({
      title:res['validation errors'].email,
      showConfirmButton:true,
      confirmButtonText:'Ok'
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl("/user-profile", { skipLocationChange: true }).then(() => {
          console.log([decodeURI(this.location.path())]);
          this.router.navigate([decodeURI(this.location.path())])
      
        });
      }
    })
    // alert(res['validation errors'].email);
  }else if(res['validation errors'].username){
    Swal.fire({
      title:res['validation errors'].username,
      showConfirmButton:true,
      confirmButtonText:'Ok'
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl("/user-profile", { skipLocationChange: true }).then(() => {
          console.log([decodeURI(this.location.path())]);
          this.router.navigate([decodeURI(this.location.path())])
      
        });
      }
    })
    // alert(res['validation errors'].email);
  }
    // this.validation_error =
    // this.errors = res['validation errors'];
  }else{
    Swal.fire({
      title:'Success',
      text:res['message'],
      showConfirmButton:true,
      confirmButtonText:'Ok'
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl("/user-profile", { skipLocationChange: true }).then(() => {
          console.log([decodeURI(this.location.path())]);
          this.router.navigate([decodeURI(this.location.path())])
      
        });
      }
    })
    // alert(res['success'].message);
  }
})
  }

}
