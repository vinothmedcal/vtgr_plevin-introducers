import { Routes } from '@angular/router';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';

// import { IconsComponent } from '../../pages/icons/icons.component';
// import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from 'src/app/pages/cases/case_list/tables.component';
import { AddcaseComponent } from 'src/app/pages/cases/addcase/addcase.component';
import { AdduploadComponent } from 'src/app/pages/upload/addupload/addupload.component';
import { UploadComponent } from 'src/app/pages/upload/uploadlist/upload.component';
import { ChangepasswordComponent } from 'src/app/pages/changepassword/changepassword.component';
import { InboxComponent } from 'src/app/pages/inbox/inboxlist/inbox.component';
import { AddinboxComponent } from 'src/app/pages/inbox/addinbox/addinbox.component';
import { InboxviewComponent } from 'src/app/pages/inbox/inboxview/inboxview.component';
import { SendComponent } from 'src/app/pages/inbox/send/send.component';
import { DocumentviewComponent } from 'src/app/pages/upload/documentview/documentview.component';
import { BankComponent } from 'src/app/pages/bank/bank.component';
import { SentviewComponent } from 'src/app/pages/inbox/sentview/sentview.component';

export const AdminLayoutRoutes: Routes = [
    // { path: 'pba_introducers/dashboard', component: DashboardComponent },
    { path: 'profile',   component: UserProfileComponent },
    { path: 'cases',         component: TablesComponent },
    { path: 'bank',         component: BankComponent },
    {path:'addcase',component:AddcaseComponent,data:{title:'Add Case'}},
    {path:'addupload',component:AdduploadComponent},
    {path:'addinbox',component:AddinboxComponent},
    {path:'changepassword',component:ChangepasswordComponent},
    { path: 'inbox',          component: InboxComponent },
    { path: 'inboxview',          component: InboxviewComponent },
    { path: 'sentview',          component: SentviewComponent },
    { path: 'send',          component: SendComponent },
    { path: 'upload',           component: UploadComponent },
    { path: 'documentview',           component: DocumentviewComponent },

];
