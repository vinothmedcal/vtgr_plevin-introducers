import {NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFileUploaderModule } from "angular-file-uploader";
// import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';


// import { DashboardComponent } from '../../pages/dashboard/dashboard.component';

// import { IconsComponent } from '../../pages/icons/icons.component';
// import { MapsComponent } from '../../pages/maps/maps.component';
// import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddcaseComponent } from 'src/app/pages/cases/addcase/addcase.component';
import { TablesComponent } from 'src/app/pages/cases/case_list/tables.component';
import { UploadComponent } from 'src/app/pages/upload/uploadlist/upload.component';
import { AdduploadComponent } from 'src/app/pages/upload/addupload/addupload.component';
import { ngfModule, ngf } from "angular-file"
import { UserProfileComponent } from 'src/app/pages/user-profile/user-profile.component';
import { ChangepasswordComponent } from 'src/app/pages/changepassword/changepassword.component';
import { ResetpasswordComponent } from 'src/app/pages/resetpassword/resetpassword.component';
import { InboxComponent } from 'src/app/pages/inbox/inboxlist/inbox.component';
import { AddinboxComponent } from 'src/app/pages/inbox/addinbox/addinbox.component';
import { InboxviewComponent } from 'src/app/pages/inbox/inboxview/inboxview.component';
import { SendComponent } from 'src/app/pages/inbox/send/send.component';
import { DataTablesModule } from 'angular-datatables';
// import { ToastrModule } from 'ngx-toastr';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SentviewComponent } from 'src/app/pages/inbox/sentview/sentview.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgxTypeaheadModule } from 'ngx-typeahead';
import { SafepipePipe } from 'src/app/pages/safepipe.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    AngularFileUploaderModule,
    ngfModule,
    ReactiveFormsModule ,
    DataTablesModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AutocompleteLibModule,
    MatInputModule,MatFormFieldModule,
    MatAutocompleteModule,
    NgxTypeaheadModule
    // ClipboardModule
  ],
  declarations: [
    // DashboardComponent,
    AddcaseComponent,
    UploadComponent,
    AdduploadComponent,
    ChangepasswordComponent,
    ResetpasswordComponent,
    InboxComponent,
    InboxviewComponent,
    // UserProfileComponent,
    TablesComponent,
    UserProfileComponent,
    AddinboxComponent,
    SendComponent,
    SentviewComponent,
    SafepipePipe
    // IconsComponent,
    // MapsComponent
  ]
})

export class AdminLayoutModule {}

