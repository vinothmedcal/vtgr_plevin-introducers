import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ApiconfigService } from './apiconfig.service';
import { catchError, retry, tap, debounceTime } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  public _URL: string = "";
  logininfo:any;
  constructor(private apiser: ApiconfigService,private http:HttpClient) {
    if(sessionStorage.getItem('login_details')){
      this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
      }

     
   }
  //  getvitigerapi() {
  //   return 'https://medcalconsulting.od2.vtiger.com/';
  // }
    getServiceAPI(): string {
      return sessionStorage.getItem('api_url');
    }
  
    getpostcodeAPI(): string {
      return sessionStorage.getItem('postcode_api');
    }

    getpostcodekey(): string {
      return sessionStorage.getItem('POSTCODE_KEY');
    }
   
    seesionuser_info(res){
      sessionStorage.setItem('login_details',JSON.stringify(res));
  }

  // vitigerpost(service:string, data: any) {
  //   let options = {
  //     headers :new HttpHeaders({'Accept': 'application/json'}).
  //   set('Access-Control-Allow-Methods','POST, GET, OPTIONS, DELETE, PUT').
  //   set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT').
  //   set('Access-Control-Allow-Origin', '*').
  //   set( 'Access-Control-Allow-Headers' ,"X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding")
  //   };

    
   
  //     // ... Set content type to JSON
   
  //   // console.log("headers1: value" + JSON.stringify(headers));
   
  //   return this.http.post(this.getvitigerapi()+service, data,options);  
  //   // let httpOptions = {
  //   //   headers: new HttpHeaders({
  //   //       'Content-Type': 'application/json'
  //   //   })
  //   // }
    
  //   let URL = this._URL;
  //   return this.http.post(URL,data);
  // }

  loginpost(Service: string, data: any) {
    // console.log(this.logininfo);
    let httpOptions;

    this._URL = this.getServiceAPI();

    let headers = new Headers({ 'Content-Type': 'application/json' });
    
    let URL = this._URL + Service;
    return this.http.post(URL, data);
  }

  postRecords(Service: string, data: any,isFileUpload=false) {
    // console.log(this.logininfo);
    let httpOptions;
    if(sessionStorage.getItem('login_details')){
      this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    }
   if(this.logininfo === null){
     httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
    }
   }else{
   httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      }).set('Authorization',  'Bearer'+' '+this.logininfo['success'].token)
    }
  }
    this._URL = this.getServiceAPI();

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let URL = this._URL + Service;
    if(isFileUpload){
      httpOptions = {
        headers: new HttpHeaders({
        }).set('Authorization',  'Bearer'+' '+this.logininfo['success'].token)
      }
      return this.http.post(URL, data,httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )

    } else{
      return this.http.post(URL, data, httpOptions);

    }
  }

  getpostcode(service:any,term:any) {
    this._URL = this.getpostcodeAPI();
   let key = this.getpostcodekey();

    let  URL = this._URL+service+key+term;
  

    var postcode = this.http.get(URL)
    .pipe(
        debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
        map(
            (data: any) => {
                return (
                    data.length != 0 ? data as any[] : [{"BookName": "No Record Found"} as any]
                );
            }
    ));

    return postcode;  
    // this._URL = this.getpostcodeAPI();
    // let URL = Service;
    // return this.http.get(URL);
  }

  postDelete(Service: string,data:any) {
    this._URL = this.getServiceAPI();
  //console.log("docme url" + '-' + this._URL);
 
  if(sessionStorage.getItem('login_details')){
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
  }
  let httpOptions = {
    headers: new Headers({'Content-Type': 'application/json'}).set('Authorization',  'Bearer'+' '+this.logininfo['success'].token)
  }
  // let headers = new Headers({ 'Content-Type': 'application/json' }).set('Authorization',  'Bearer'+' '+this.logininfo['success'].token);
    
    // console.log(httpOptions);
    // return;
    let URL = this._URL + Service;
    return this.http.post(URL, httpOptions)
        .pipe(retry(1),
          catchError(this.handleError)
      )

  }

  getrecord(Service: string, data: any): any {
    this._URL = this.getServiceAPI();
    //console.log("docme url" + '-' + this._URL);

    let headers = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Authorization',  data)
    }
    let URL = this._URL + Service;
    return this.http.get(URL, headers);
}

getresetrecord(Service: string): any {
  this._URL = this.getServiceAPI();
  //console.log("docme url" + '-' + this._URL);

  let headers = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  let URL = this._URL + Service;
  return this.http.get(URL, headers);
}

deleterecord(Service: string): any {
  this._URL = this.getServiceAPI();
  //console.log("docme url" + '-' + this._URL);

  let  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    }).set('Authorization',  'Bearer'+' '+this.logininfo['success'].token)
  }
  let URL = this._URL + Service;
  return this.http.delete(URL, httpOptions);
}

data(data:any){
  return data;
}

private handleError(errorResponse: HttpErrorResponse) {
  if (errorResponse.error instanceof ErrorEvent) {
      console.error('client side error :', errorResponse.error.message);
  } else {
    // console.error(errorResponse.error);
      return errorResponse.error;
  }
  return throwError('ther is problem with service');
}

logout(){
  sessionStorage.removeItem('login_details');
  // sessionStorage.removeItem('api_url');

}
}
